# ROME RDF

# SCHEME
```
@PREFIX mm: <https://data.mindmatcher.org/ontology/>
@PREFIX ami: <http://openemploi.datasud.fr/ontology/>

<http://openemploi.datasud.fr/data/scheme/1> a skos:ConceptScheme;
    skos:prefLabel "Domaines Professionels"@fr.

<http://openemploi.datasud.fr/data/concept/A> a skos:Concept;
    skos:prefLabel "Agriculture et Pêche, Espaces naturels et Espaces verts, Soins aux animaux"@fr;
    skos:notation "A"@fr;
    skos:inScheme <http://openemploi.datasud.fr/data/scheme/1> .

<http://openemploi.datasud.fr/data/scheme/1> skos:hasTopConcept <http://openemploi.datasud.fr/data/concept/A> .

<http://openemploi.datasud.fr/data/concept/A11> a skos:Concept;
    skos:prefLabel "Engins agricoles et forestiers"@fr;
    skos:notation "A11"@fr;
    skos:broader <http://openemploi.datasud.fr/data/concept/A>;
    skos:inScheme <http://openemploi.datasud.fr/data/scheme/1> .

<http://openemploi.datasud.fr/data/concept/A1101> a skos:Concept;
    skos:prefLabel "Conduite d'engins agricoles et forestiers"@fr;
    skos:notation "A1101"@fr;
    skos:broader <http://openemploi.datasud.fr/data/concept/A11>;
    skos:inScheme <http://openemploi.datasud.fr/data/scheme/1> .






<http://openemploi.datasud.fr/data/scheme/2> a skos:ConceptScheme;
    skos:prefLabel "Métiers"@fr.

<http://openemploi.datasud.fr/data/scheme/3> a skos:ConceptScheme;
    skos:prefLabel "Compétences"@fr.

<http://openemploi.datasud.fr/data/scheme/4> a skos:ConceptScheme;
    skos:prefLabel "Environnements de travail"@fr.

```
# Occupation
```
@PREFIX mm: <https://data.mindmatcher.org/ontology/>
@PREFIX ami: <http://openemploi.datasud.fr/ontology/>

<http://openemploi.datasud.fr/data/occupation/38877> a mm:Occupation;
    skos:prefLabel "Conducteur / Conductrice d'épareuse"@fr;
    skos:altLabel "Conducteur d'épareuse"@fr;
    skos:altLabel "Conductrice d'épareuse"@fr;
    skos:hiddenLabel "conduct* epareuse"@fr;
    skos:related <http://openemploi.datasud.fr/data/concept/A1101>;
    skos:inScheme <http://openemploi.datasud.fr/data/scheme/2> .

```

# Skill
```
@PREFIX mm: <https://data.mindmatcher.org/ontology/>
@PREFIX ami: <http://openemploi.datasud.fr/ontology/>


<http://openemploi.datasud.fr/data/skill/00007> a mm:Skill;
    skos:prefLabel "Compétences transverses"@fr;
    skos:hiddenLabel "competences transverses"@fr;
    skos:inScheme <http://openemploi.datasud.fr/data/scheme/3> .

<http://openemploi.datasud.fr/data/scheme/3> skos:hasTopConcept <http://openemploi.datasud.fr/data/skill/00007> .

<http://openemploi.datasud.fr/data/skill/00222> a mm:Skill;
    skos:prefLabel "Organisation d'une action ou pilotage d'un projet"@fr;
    skos:hiddenLabel "organiser action"@fr;
    skos:hiddenLabel "pilotage projet"@fr;
    skos:broader <http://openemploi.datasud.fr/data/concept/00007>;
    skos:inScheme <http://openemploi.datasud.fr/data/scheme/3> .

<http://openemploi.datasud.fr/data/skill/122704> a mm:Skill;
    skos:prefLabel "Identifier le type d'intervention"@fr;
    skos:hiddenLabel "identifier type intervention"@fr;
    skos:broader <http://openemploi.datasud.fr/data/concept/00007>;
    skos:related <http://openemploi.datasud.fr/data/concept/00222>;
    skos:inScheme <http://openemploi.datasud.fr/data/scheme/3> .


```








```
$.competencesDeBase[0].code = "125978"
$.competencesDeBase[0].noeudCompetence.code = "00173"
$.competencesDeBase[0].noeudCompetence.racineCompetence = "00173"

mm:Skill/md5(125978) a mm:Skill ; rdf:type skos:concept ; skos:inScheme ami:scheme/skill ; skos:related mm:occupation/md5(A1101) .
mm:Skill/md5(00173) a mm:Skill ; rdf:type skos:concept ; skos:inScheme ami:scheme/skill  ; skos:related mm:occupation/md5(A1101) .
```
Same thing to do with -> groupesCompetencesSpecifiques

# Working Environnement
```
$.environnementsTravail[0].code = "23833"
```



```
$.code = "A1101"
$.domaineProfessionel.code = "A11"
$.domaineProfessionel.grandDomaine.code = "A"


mm:occupation/md5(A1101) rdf:type mm:occupation ;
mm:occupation/md5(A1101) rdf:type skos:concept ;

mm:occupation/md5(A11) rdf:type mm:occupation ;
mm:occupation/md5(A11) rdf:type skos:concept ;

mm:occupation/md5(A) rdf:type mm:occupation ;
mm:occupation/md5(A) rdf:type skos:concept ;

ami:scheme/occupation skos:hasTopConcept mm:occupation/md5(A) ;

mm:occupation/md5(A11) skos:broader mm:occupation/md5(A) ;
mm:occupation/md5(A1101) skos:broader mm:occupation/md5(A11) ;

$.mobilitesEvolutionsVersMetiers[0].metierCible.code = "I1603"
$.mobilitesProchesVersMetiers.metierCible.code = "A1416" 

mm:occupation/md5(I1603) rdf:type mm:occupation ;
mm:occupation/md5(I1603) rdf:type skos:concept ;

mm:occupation/md5(A1416) rdf:type mm:occupation ;
mm:occupation/md5(A1416) rdf:type skos:concept ;
```

ami:WorkingEnvironnement/md5(23833) rdf:type skos:concept ; skos:inScheme ami:scheme/workingEnvironnement ; skos:related mm:occupation/md5(A1101) .